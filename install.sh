## update packages
sudo pacman -Syyuu

## install
sudo pacman -S rofi qtile emacs firefox terminator syncthing r fish vlc gcc-fortran gdal arrow aspell-en xorg-xmodmap

## stuff from aur
yay -S udunits # this is for sf in R

## for backlight on laptop
# install the backlight package (I don't know where it's at)
sudo echo $'Section "Device"\nIdentifier  "Intel Graphics"\nDriver      "intel"\nOption      "Backlight"  "intel_backlight"\nEndSection' > /etc/X11/xorg.conf

# git configuration
git config --global user.email "haffner.matthew.m@gmail.com"
git config --global user.name "Matthew Haffner"

## get dotfiles into home directory
## TODO confirm that this works
cd /home/matt
git init
git remote add origin https://gitlab.com/mhaffner/dotfiles
git fetch
git checkout master -f

## start and enable syncthing
systemctl start syncthing --user
systemctl enable syncthing --user

## install oh-my-fish
curl -L https://get.oh-my.fish | fish

## install bobthefish theme
omf install bobthefish

## change default shell to fish
chsh -s `which fish` matt

## make git-repos directory
mkdir -p /home/matt/git-repos

## spacemacs
git clone -b develop https://github.com/syl20bnr/spacemacs ~/.emacs.d
